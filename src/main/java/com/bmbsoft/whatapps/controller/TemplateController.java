package com.bmbsoft.whatapps.controller;

import com.bmbsoft.whatapps.model.entity.Template;
import com.bmbsoft.whatapps.service.TemplateService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/template")
public class TemplateController {

  @Autowired
  TemplateService templateService;

  @PostMapping("/save")
  public ResponseEntity<Template> saveTemplate(@RequestBody Template template) {
    try {
      ObjectMapper mapper = new ObjectMapper();

      JsonNode rootNode = mapper.readTree(template.getTemplateData());

      ((ObjectNode) rootNode).put("to", "${to}");
      template.setTemplateData(
          mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode));
      return new ResponseEntity<>(templateService.saveTemplate(template), HttpStatus.OK);
    } catch (Exception e) {
      e.printStackTrace();
      return new ResponseEntity<>(HttpStatus.OK);
    }
  }

  @GetMapping("/find-all")
  public ResponseEntity<?> findAll() {
    return new ResponseEntity<>(templateService.findAllTemplate(), HttpStatus.OK);
  }

  @DeleteMapping("/delete")
  public ResponseEntity<?> deleteByID(@RequestBody Template template) {
    templateService.deleteTemplate(template.getId());
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
