package com.bmbsoft.whatapps.controller;


import com.bmbsoft.whatapps.model.WhatAppResponse;
import com.bmbsoft.whatapps.service.WhatsappBotService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
@RequestMapping("/webhook")
public class WebhookController {

    @Autowired
    WhatsappBotService botService;

    @Autowired
    ObjectMapper mapper;

    private static final String VERIFY_TOKEN = "bmb_soft";

    Logger logger = LoggerFactory.getLogger(WebhookController.class);

    @PostMapping
    public ResponseEntity<String> webhook(@RequestBody String data) throws JsonProcessingException {
        logger.info("Raw Data: " + data);
        WhatAppResponse response = mapper.readValue(data, WhatAppResponse.class);
        logger.info(new JSONObject(response).toString());
        botService.executeMessage(response);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @GetMapping()
    public String verifyWebhook(
            @RequestParam(name = "hub.mode") String mode,
            @RequestParam(name = "hub.challenge") String challenge,
            @RequestParam(name = "hub.verify_token") String token) {
        logger.info("Start test connection: {}", mode);
        if (mode.equals("subscribe") && token.equals(VERIFY_TOKEN)) {
            return challenge;
        } else {
            return "Verification failed";
        }
    }
}
