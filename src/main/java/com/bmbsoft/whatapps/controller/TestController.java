package com.bmbsoft.whatapps.controller;


import com.bmbsoft.whatapps.model.entity.Test;
import com.bmbsoft.whatapps.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
    TestService testService;

    @PostMapping("/save")
    public ResponseEntity<?> saveTest(@RequestBody Test test){
        return new ResponseEntity<>(testService.saveTest(test), HttpStatus.OK);
    }
    @GetMapping("/get")
    public ResponseEntity<?> getTest(){
        return new ResponseEntity<>(testService.findAll(), HttpStatus.OK);
    }

}
