package com.bmbsoft.whatapps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class WhatappsApplication {

  public static void main(String[] args) {
    SpringApplication.run(WhatappsApplication.class, args);
  }

}
