package com.bmbsoft.whatapps.service;

import com.bmbsoft.whatapps.model.WhatAppResponse;


public interface WhatsappBotService {

  void executeMessage(WhatAppResponse response);

  void sendMessage(String resourceString);

}
