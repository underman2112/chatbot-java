package com.bmbsoft.whatapps.service;

import com.bmbsoft.whatapps.model.entity.Test;

import java.util.List;

public interface TestService {
    Test saveTest(Test test);

    List<Test> findAll();
}
