package com.bmbsoft.whatapps.service;

import com.bmbsoft.whatapps.model.entity.TemplateFlow;

import java.util.List;

public interface TemplateFlowService {
  public String getTheNextTemplate(String optionType, String optionText);

  TemplateFlow findByTemplateFlow(String optionType, String optionText);
}
