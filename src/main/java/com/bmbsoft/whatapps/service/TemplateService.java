package com.bmbsoft.whatapps.service;

import com.bmbsoft.whatapps.model.ChangeTo;
import com.bmbsoft.whatapps.model.entity.Template;
import java.util.List;

public interface TemplateService {
    Template saveTemplate(Template template);
    List<Template> findAllTemplate();
    void deleteTemplate(String id);
    Template findByID(String id);
    Template findByCode(String code);

    String changeTo(ChangeTo change);
}
