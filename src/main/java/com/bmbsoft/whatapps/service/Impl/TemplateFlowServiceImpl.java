package com.bmbsoft.whatapps.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bmbsoft.whatapps.model.entity.TemplateFlow;
import com.bmbsoft.whatapps.repository.TemplateFlowRepository;
import com.bmbsoft.whatapps.service.TemplateFlowService;

import java.util.List;

@Service
public class TemplateFlowServiceImpl implements TemplateFlowService {

  @Autowired
  private TemplateFlowRepository repository;

  @Override
  public String getTheNextTemplate(String optionType, String optionText) {
    TemplateFlow flow = repository.findByOptionTypeAndOptionText(optionType, optionText);

    return flow.getNextTemplateCode();
  }

  @Override
  public TemplateFlow findByTemplateFlow(String optionType, String optionText) {
    return repository.findTemplateFlowByOptionTypeAndOptionText(optionType, optionText);
  }

}
