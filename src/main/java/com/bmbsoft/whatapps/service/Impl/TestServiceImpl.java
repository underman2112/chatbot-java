package com.bmbsoft.whatapps.service.Impl;

import com.bmbsoft.whatapps.model.entity.Test;
import com.bmbsoft.whatapps.repository.ExampleRepository;
import com.bmbsoft.whatapps.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {
    @Autowired
    ExampleRepository exampleRepository;
    @Override
    public Test saveTest(Test test) {
       return exampleRepository.save(test);
    }

    @Override
    public List<Test> findAll() {
        return exampleRepository.findAll();
    }
}
