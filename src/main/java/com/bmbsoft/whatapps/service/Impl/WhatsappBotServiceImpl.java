package com.bmbsoft.whatapps.service.Impl;


import com.bmbsoft.whatapps.model.*;
import com.bmbsoft.whatapps.model.entity.Template;
import com.bmbsoft.whatapps.model.entity.TemplateFlow;
import com.bmbsoft.whatapps.service.TemplateFlowService;
import com.bmbsoft.whatapps.service.TemplateService;
import com.bmbsoft.whatapps.service.WhatsappBotService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static com.bmbsoft.whatapps.constant.TemplateType.*;


@Service
@RequiredArgsConstructor
@Slf4j
public class WhatsappBotServiceImpl implements WhatsappBotService {

    private final TemplateService tempService;
    private final TemplateFlowService flowService;
    @Value("${authorization.token}")
    private String token;
    @Value("${authorization.uri}")
    private String uri;

    @Override
    public void executeMessage(WhatAppResponse response) {
        ResponseValue value = response.getEntry().get(0).getChanges().get(0).getValue();
        String waId = Optional.ofNullable(value.getContacts())
                .map(contacts -> contacts.get(0).getWaId())
                .orElse("");
        Optional.ofNullable(value.getMessages())
                .map(messages -> messages.get(0))
                .map(message -> getTemplate(message, waId))
                .ifPresent(this::sendMessage);
    }

    @Override
    public void sendMessage(String resourceString) {
        if (resourceString == null) {
            return;
        }
        log.info(resourceString);
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> request = new HttpEntity<>(resourceString, headers);
        log.info("Request: " + request);
        String result = restTemplate.postForObject(uri, request, String.class);
        log.info("Send request: " + result);
    }

    private String getStringTemplate(String type, String text, String waId) {
        TemplateFlow flow = flowService.findByTemplateFlow(type, text);
        if (!ObjectUtils.isEmpty(flow)) {
            Template template = tempService.findByCode(flow.getCode());
            ChangeTo change = ChangeTo.builder()
                    .id(template.getId())
                    .to(waId)
                    .build();
            return tempService.changeTo(change);
        }
        return "";
    }

    private String getTemplate(Message message, String waId) {
        String text = "";
        if (TEXT.getValue().equals(message.getType())) {
            text = message.getText().getBody();
        } else if (INTERACTIVE.getValue().equals(message.getType())) {
            text = message.getInteractive().getButtonReply().getId();
        }
        return this.getStringTemplate(message.getType(), text, waId);
    }
}
