package com.bmbsoft.whatapps.service.Impl;

import com.bmbsoft.whatapps.model.ChangeTo;
import com.bmbsoft.whatapps.model.entity.Template;
import com.bmbsoft.whatapps.model.entity.Test;
import com.bmbsoft.whatapps.repository.ExampleRepository;
import com.bmbsoft.whatapps.repository.TemplateRepository;
import com.bmbsoft.whatapps.service.TemplateService;
import com.bmbsoft.whatapps.service.TestService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.core.lookup.StrSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TemplateServiceImpl implements TemplateService {
    @Autowired
    TemplateRepository templateRepository;

    @Override
    public Template saveTemplate(Template template) {
        return templateRepository.save(template);
    }

    @Override
    public List<Template> findAllTemplate() {
        return templateRepository.findAll();
    }

    @Override
    public void deleteTemplate(String id) {
        templateRepository.deleteById(id);
    }

    @Override
    public Template findByID(String id) {
        return templateRepository.findById(id).orElse(null);
    }

    @Override
    public Template findByCode(String code){
        return templateRepository.findByCode(code);
    }

    @Override
    public String changeTo(ChangeTo change) {
        Template templateByID = this.findByID(change.getId());
        return Optional.ofNullable(templateByID).map(template -> {
            Map<String, String> data = new HashMap<>();
            data.put("to", change.getTo());
            String formattedString = StrSubstitutor.replace(template.getTemplateData(), data);
            log.info(formattedString);
            return formattedString;
        }).orElse(null);
    }
}
