package com.bmbsoft.whatapps.repository;

import com.bmbsoft.whatapps.model.entity.Test;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExampleRepository extends MongoRepository<Test, Long> {

}
