package com.bmbsoft.whatapps.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;
import com.bmbsoft.whatapps.model.entity.TemplateFlow;

import java.util.List;

@Repository
public interface TemplateFlowRepository extends MongoRepository<TemplateFlow, String> {
  TemplateFlow findByOptionTypeAndOptionText(String optionType, String optionText);

  @Query("{ 'optionType' : ?0 , 'optionText' : ?1}")
  TemplateFlow findTemplateFlowByOptionTypeAndOptionText(String optionType, String optionText);
}
