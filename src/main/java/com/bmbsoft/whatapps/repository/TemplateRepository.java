package com.bmbsoft.whatapps.repository;

import com.bmbsoft.whatapps.model.entity.Template;
import com.bmbsoft.whatapps.model.entity.Test;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateRepository extends MongoRepository<Template, String> {
    Template findByCode(String templateCode);

}
