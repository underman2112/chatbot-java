package com.bmbsoft.whatapps.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ChangeTo {
  private String id;
  private String to;

}
