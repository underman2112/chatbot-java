package com.bmbsoft.whatapps.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Button {
  
    private String payload;
    
    private String text; 

}
