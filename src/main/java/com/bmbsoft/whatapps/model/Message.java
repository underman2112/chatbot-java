package com.bmbsoft.whatapps.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {
    private Context context;
    private String from;
    
    private String id;
    
    private String timestamp;
    
    private Text text;
    
    private String type;
    
    private Button button;

    private Interactive interactive;

}
