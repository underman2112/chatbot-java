package com.bmbsoft.whatapps.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Interactive {
    private String type;
    @JsonProperty("button_reply")
    private ButtonReply buttonReply;

    @Getter
    @Setter
    public static class ButtonReply {
        private String id;
        private String title;
    }
}
