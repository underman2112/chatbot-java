package com.bmbsoft.whatapps.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class WhatAppResponse {
    private String object;
    
    private List<ResponseEntry> entry;

}
