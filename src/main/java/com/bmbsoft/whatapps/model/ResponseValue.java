package com.bmbsoft.whatapps.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseValue {

    
    @JsonProperty("messaging_product")
    private String messagingProduct;
    
    private Metadata metadata;

    @JsonProperty("contacts")
    private List<Contact> contacts;
    @JsonProperty("messages")
    private List<Message> messages;
    @JsonProperty("statuses")
    private List<Status> statuses;
    


   
}
