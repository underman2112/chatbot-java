package com.bmbsoft.whatapps.model.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import com.bmbsoft.whatapps.model.ERPInformation;
import lombok.Getter;
import lombok.Setter;

@Document("template_flow")
@Getter
@Setter
public class TemplateFlow extends BaseEntity {
  /**
   * 
   */
  private static final long serialVersionUID = -2633751443428925581L;

  private String code;

  private Integer level;

  private String optionText;

  private String optionType;

  private String nextTemplateCode;

  private ERPInformation info;

}
