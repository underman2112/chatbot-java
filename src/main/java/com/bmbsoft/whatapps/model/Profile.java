package com.bmbsoft.whatapps.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Profile {

    private String name;
}
