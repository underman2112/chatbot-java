package com.bmbsoft.whatapps.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResponseEntry {

    private String id;
    
    private List<ResponseChange> changes;




}
