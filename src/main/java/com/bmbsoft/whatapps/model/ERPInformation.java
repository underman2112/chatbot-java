package com.bmbsoft.whatapps.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ERPInformation {
  private String service;

  private String shop;

  private String product;
}
