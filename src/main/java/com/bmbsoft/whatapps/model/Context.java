package com.bmbsoft.whatapps.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Context {
    private String from;
    private String id;
}
