package com.bmbsoft.whatapps.constant;

public enum TemplateType {
    WHATSAPP_BUSINESS_ACCOUNT("whatsapp_business_account"),
    TEXT("text"),
    INTERACTIVE("interactive"),
    BUTTON("button"),
    SHOW_ME_YOUR_SERVICE("Show me your service"),
    PLACE_ORDER("place_order"),
    SHOP_A("shop_a")
    ;


    private String value;

    TemplateType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
