package com.bmbsoft.whatapps.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

@Configuration
public class DatabaseConfig {
    @Value("${spring.data.mongodb.uri}")
    private String uri;
    private final Logger logger = LoggerFactory.getLogger(DatabaseConfig.class);

    @Bean
    public MongoDatabaseFactory mongoDatabaseFactory() {
        logger.info("configuring mongo db template !!!");
        return new SimpleMongoClientDatabaseFactory(uri);
    }
}
